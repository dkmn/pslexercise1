# Cache Coding Exercise

David K. Kim

![Robot Reading](./Robot-reading-book-2-smaller.jpg)

## Summary
Simple Java application implementing a cache for sale tax rate lookups by address. 
Implemented with some build and packaging automation to potentially simplify evaluation. 
Thoughts and design notes are reflected in this document (written after implementation) and in-line as comments. 

## Build and Run
Clone this repository into a good working location:
```
git clone https://bitbucket.org/dkmn/pslexercise1
cd pslexercise1
```
A Dockerfile is provided for ease of setup or cross-platform evaluation. 

After installing Docker, if need be, from the repository directory do: 

```
docker build -t pslexercise1 .  
docker run -it pslexercise1
```
this will build the project within the container, downloading packages as necessary, and run the Main.main() function 
which performs a simple test of the implemented caching function with a synthetic 10,000,000 address cohort (without 
display) for rough timing, followed by a small 10,000 address test with console-logging to show the queries.  

Build definition is provided via Maven, and it may be simpler to just import the repository into 
your IDE of choice (e.g. IntelliJ) and run it locally, particularly to interrogate code or examine structures via 
debugger. 

The intended class to run for demo/test is:
```
net.radardoc.psl.Main
```

To build and run manually at the command line, from the repository base directory, do:  
```
mvn clean package
java -jar target/pslexercise1-1.0-SNAPSHOT.jar 
```
or 
```
mvn clean package
java -cp target/classes/ net.radardoc.psl.Main   
```
This presumes both Maven and a JDK have been installed (project targets Java 8 and above).  


## Background

Exercise spec: 

> This problem may be solved using any programming language. It's a made-up problem to test your technical skills, and should take approximately an hour to complete.
> 
> BACKGROUND
> 
> Determining the sales tax rate at a given address is an expensive operation. Our team has written a function called sales_tax_lookup, that takes a street address as a parameter and returns the sales tax rate, but it is slow to return a result.
> 
> PROBLEM
> 
> While there are billions of addresses, we've identified that 95% of lookups come from a relatively small number of addresses (less than 100K addresses). Unfortunately, you won't know what these addresses are in advance.
> 
> You have been tasked with writing a caching layer, with a constraint that the cache can only be large enough to store approximately 50K addresses. Your goal is to produce a function "fast_rate_lookup" that returns the same results as "sales_tax_lookup", but with better performance.
> 
> NOTES
> Optimize for the fewest number of calls to sales_tax_lookup while still maintaining full functionality.
> It is okay for the cache to start empty, and to fill over time. There is no need to pre-warm the cache when the server reboots.
> Assume this will run on a single server, and that the cache can be stored in program memory (i.e., a persistent variable is okay for the exercise. 

Given the real-world context of the exercise, I've focused on demonstrating some algorithm-coding, but also 
try to discuss design and architectural considerations closer to practice, deployment, and suitability.  

## Key Assumptions

* The distribution of queries across addresses (i.e. which are the 100K "frequently-queried") is independent of _time_ across a 
reasonable timescale. This would need to be verified in practice, as different caching strategies might apply:
  
   * Removing least-recently used (LRU) items is implemented here, and would be good for random distributions of the 
   "frequently-queried" sub-population. This would also work for slow shifts in the overall "frequently-queried" 
   sub-population. 
   * If these "frequently-queried" addresses oscillate quickly enough, with enough intervening cache misses to empty 
   the cache, there would be a lag in re-optimization, which might be less of a problem if frequency of access were 
   tracked (e.g. this address hasn't been queried this week but is "frequently-queried" over a month and thus 
   should be removed relatively later than an address we haven't see at all in a year). E.g. an LFU cache could be used. 
   * Practically, would start with a best assessment of the temporal distribution (by retrospective analysis of smart guessing 
   in a totally new use case), and then watch cache composition and hit-rates over time. 
   
* No other correlated information is known about these addresses. In practice, might look for geographic, length, or 
other correlations which might predict frequency. 

## Design and Implementation Notes

I've chosen Java for this exercise given:

* good personal familiarity over time (used heavily in past, though writing less often currently)
* widely-understood and reasonably-structured 3rd-gen language; type and structure assumptions should be apparent
* the JVM and Java ecosystem (software and human) are such that it would be a viable candidate for production

### General Design Considerations
* The pre-existing sales_tax_lookup function is specified as slow and also as "expensive"; this could be synonymous 
with the high time cost or reflect other costs (SaaS fees, UX impacts, complexity relating to tuning multiple services, 
etc). Regardless, the cache should be reasonably fast in terms of fixed time-cost and also have favorable temporal 
scaling with size. 
* Given scope of this exercise, I've leveraged core Java libraries (e.g. Collections) whenever possible, in order 
to show some algorithm coding. In practice, finding a well-supported open-source implementation of a more specific 
cache might be a better use of human time (and also avoid some potential pitfalls with re-invention esp. subtle 
implementation complexities of Collection performance, threading, etc).  But a library import and one-line call would 
be boring for you to look at. :)
* Multi-threading would be desired, esp. in a single-server, in-memory solution. Given the scope of this exercise, I 
am not implementing this. If needed, would try to leverage synchronized versions of these Collections or use a known 
library. 
* I've created an Address object to reflect some minimal abstraction, to hint that addresses may be reflected in 
pre-parsed, normalized, or structured forms. But, for the purpose of this exercise, a simple String-based  
representation is used for both the internal data and the value to be used for cache hashing. 
* Abstracting the Cache behavior with an Interface would also be reasonable and more easily facilitate swapping and 
testing of different cache types and implementations (e.g. LFU, something fancier based on time periods, etc). 

 
### Additional thoughts during and after this exercise
* Wrote a simple test cohort generator that mixes a few "frequently-queried" addresses with pseudo-random ones.  
In larger samples, I am seeing hit rates asymptotically hover near 0.95, which implies a good fit for a random or 
pseudo-random temporal distribution. 

### Minor notes
* I've Javafied the function names (camelCase instead of snake_case)
* The main function runs a default batch of 10,000,000 queries with 95% of queries coming from a particular fixed 
subset to get a rough estimate of execution time. Subsequently, then runs a smaller batch of 10,000 queries with this 
pre-warmed cache, with console logging to show some query traffic. 
Note that using this to play with different cache dynamics currently requires changing 
constants or instance variables as appropriate, but is pretty easy to do in the debugger (BTW decreasing the 
CACHE_RECORD_LIMIT facilitates the qualitative feedback with such experiments). 
