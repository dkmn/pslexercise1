package net.radardoc.psl;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        // instantiate the cache service
        CachedLookupService cachedLookupService = new CachedLookupService();
        // a reminder for test/demo
        System.out.println("CACHE_RECORD_LIMIT is currently:" + CachedLookupService.CACHE_RECORD_LIMIT);

        // Set up test utility class
        RandomTestUtil testUtil = new RandomTestUtil();
        //testUtil.fractionFromFrequentQueriers = 0.95f;    // change this if desired to see different cache dynamics

        // FIRST query a large number of addresses to get a rough idea of performance
        testUtil.numberOfTestQueries = new Integer(10000000);     // change this if desired
        // Get synthetic test data using utility class
        ArrayList<Address> testList1 = testUtil.getMixedTestAddressCohort();

        Iterator<Address> testIterator = testList1.iterator();
        Address currentAddress;

        // get rough time before
        long beforeTime = System.currentTimeMillis();
        System.out.println("Now querying. BEFORE (system clock): " + beforeTime);
        // run the queries
        Float rateResult = 0f;
        testIterator = testList1.iterator();
        while (testIterator.hasNext()) {
            currentAddress = testIterator.next();
            rateResult = cachedLookupService.fastRateLookup(currentAddress);
        }
        long afterTime = System.currentTimeMillis();
        System.out.println("AFTER (system clock): " + afterTime);
        System.out.println("->approximate run time for the above batch of queries (sec): " + (float)(afterTime-beforeTime) /1000);
        System.out.println(String.format("Final cache hit rate: %2.6f\n\n", cachedLookupService.getCacheHitRate()));

        // pause to allow reading
        System.out.println("\nPausing for 10 sec to read...");
        Thread.sleep(10000);

        // SECOND query a smaller number and console-log queries just to show function
        // NOTE that the number of queries would need to be bigger than the cache limit to test displacement/eviction
        // dynamics (in practice, making the cache limit small and looking with the debugger works well)
        // NOTE that the cache is pre-warmed from the above, at this point, so naive testing would involve creating
        // a new empty one (or adding an .empty() or .reset() function to the cache)

        testUtil.numberOfTestQueries = new Integer(10000);

        // Get new synthetic test data using utility class
        ArrayList<Address> testList2 = testUtil.getMixedTestAddressCohort();

        // pause to allow reading
        System.out.println("\nNext step is to run a smaller batch with display of each query. ");
        System.out.println("\nPausing for 5 sec to read...");
        Thread.sleep(5000);

        // run the queries
        // NOTE: all the printing is "expensive" in terms of time, but is included to show function
        // in production, thought would be given to a logging framework or use of queues in some cases
        // Re-use currentAddress holder and iterator
        testIterator = testList2.iterator();
        while (testIterator.hasNext()) {
            currentAddress = testIterator.next();
            System.out.println(
                    "Address: " + currentAddress.getAddressString() + "\t -> \t" +
                    String.format("Rate is: %2.2f percent", (cachedLookupService.fastRateLookup(currentAddress)) * 100.0));
            System.out.println(String.format("\tcurrent cache hit rate is: %2.6f", cachedLookupService.getCacheHitRate()));
        }

        System.out.println("Done");
    }
}
