package net.radardoc.psl;

import java.util.*;

/**
 * Implementation of lookup service cache to provide the specified behavior
 * NOTE: I am not trying to abstract this class into a more generally-usable form (yet)
 * In practice, would look at the utility and/or re-factor this to use a common Interface implemented by Classes that
 * needed to be hashed, including Addresses and the tax lookup use case.
 *
 * Would also consider implement the cache facility itself as an Interface.
 *
 * Strategy to be used here is based on recency of use, essentially using place order in an ordered cache to reflect
 * emerging relevance (by pop-sorting instances to the front), i.e. LRU. Implementation requires time cost of this
 * operation to be constant (or at least reasonable, e.g. linear)
 */
public class CachedLookupService {

    // constant to reflect max # of cache records (as specified in the exercise)
    public static final long CACHE_RECORD_LIMIT = 50000;

    // instance/reference to class (could be a singleton/LB in practice, or anything) to reflect the external lookup service
    ExternalLookupService externalLookupService = new ExternalLookupService();

    // all of these are for diagnostics at present and would likely be handled differently in production
    private Float cacheHitRate = 0.0f;
    private Long cacheHits = 0L;
    private Long cacheAttempts = 0L;

    // internal HashMaps
    // keys are hashedValues of address, values are the returned rates from the more expensive external lookup function
    private HashMap<String, Float> taxRateHashMap = new HashMap<String, Float>();

    // also an internal LinkedList, _will use the ordering to reflect recency of hits; and the pop-sorting behavior
    // over time will implicitly reflect the # of hits_ (NOTE: not explicitly tracking # or rate of hits per unique index)
    // assumption: LinkedList behavior should give good time-cost for moving frequently-used values around
    // the actual HashMap is still hit for every query
    private LinkedList<String> orderedQueryList = new LinkedList<String>();

    /**
     * External call, passed to that service
     *
     * @param addressToLookup
     * @return
     */
    public Float salesTaxLookup(Address addressToLookup) {
        return externalLookupService.salesTaxLookup(addressToLookup);
    }

    /**
     * This is the cache-backed version of the above.
     * Note that an actual deployed architecture might have all this stuff more separated out.
     *
     * @param addressToLookup
     * @return
     */
    public Float fastRateLookup(Address addressToLookup) {
        return tryCache(addressToLookup);
    }

    /**
     * Get cache hit rate on request.
     * @return
     */
    public Float getCacheHitRate() {
        return this.cacheHitRate;
    }

    /**
     * Work of the above cached lookup is done here.
     *
     * @param queriedAddress
     * @return
     */
    private Float tryCache(Address queriedAddress) {

        cacheAttempts++;         //TODO: can handle differently in production

        // get the key Object from the Address (String in this case, so there is some coupling)
        // TODO: abstract as any Object if desired, or commit to a more specific performance-based
        // key (e.g. crypto hash) and implement additional cases to check for collisions
        String hashableVal = queriedAddress.hashableValue();

        // uninitialized returnVal placeholder to avoid duplication buried in case logic
        Float returnVal;

        // attempt to find record already in cache
        if (taxRateHashMap.containsKey(hashableVal)) {

            // cache hit!
            this.cacheHits++;       //TODO: can handle differently in production

            // where is it
            int hashedValIndex = orderedQueryList.indexOf(hashableVal);
            // move this value to "front" of cache (remove current, add at front; should be constant time-cost)
            orderedQueryList.remove(hashedValIndex);
            orderedQueryList.addFirst(hashableVal);
            // the value comes from the HashMap, which doesn't change here
            returnVal =  taxRateHashMap.get(hashableVal);

        } else {
            // cache miss!

            // do the expensive external call
            Float retrievedTaxRate = salesTaxLookup(queriedAddress);

            // then want to add it, but ask first if the cache is at its size limit?
            if (orderedQueryList.size() >= CACHE_RECORD_LIMIT) {
                // AT CAPACITY, remove items:

                // what value is last in the cache list?
                String hashedValOfLast = orderedQueryList.getLast();

                // remove the corresponding HashMap entry
                taxRateHashMap.remove(hashedValOfLast);
                // remove the the last value from the linked list
                orderedQueryList.removeLast();
            } else {
                // still room!
            }

            // still handling cache miss, so...
            // add new value to front (optimistic)
            orderedQueryList.addFirst(hashableVal);
            // get and add the actual (tax) value to the HashMap
            taxRateHashMap.put(hashableVal, retrievedTaxRate);

            returnVal =  retrievedTaxRate;
        }

        // update cache hit rate
        this.cacheHitRate = cacheHits.floatValue() / cacheAttempts.floatValue();

        return returnVal;
    }
}
