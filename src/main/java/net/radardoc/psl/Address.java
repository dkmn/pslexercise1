package net.radardoc.psl;

/**
 * class to abstract away details of an Address that would be submitted for lookup
 *   e.g. separate street, street #, city, etc, as well as normalized values for any of these
 */
public class Address {

    // Address may be more complex in practice, as hinted above.
    // BUT, for now, let's go with a simple string since the focus of this exercise is elsewhere
    private String addressString;

    // constructors
    Address() {
        this.addressString = "";
    }
    Address(String initialStringValue) {
        this.addressString = initialStringValue;
    }

    // getter
    public String getAddressString() {
        return this.addressString;
    }

    // Omitting a direct setter for a more immutable Scala-like approach
    // thus, need to create a new Address object for a new address :)

    /**
     * hashableValue for use in the cache
     *
     * Name reflects that we might want some kind of derived value, per address, to detect duplication
     * of addresses in the cache, or effective duplication elsewhere (e.g. if "different" addresses could be
     * normalized to the same address even before submission)
     * Note this is outside of any default internal _generic_ caching algorithms used in general hashes or collections.
     * e.g. .hashCode()
     *
     * Could also help performance, if the Address object is complicated; probably pre-compute in that case per-Address.
     *
     * A more condensed hash might work but would risk some collisions
     * (e.g. a crypto hash (something with low collision probability), but need to understand the risk/reward
     * or implement logic to cross-check hits.
     *
     * EXERCISE: To reflect the simplifying assumption above for this exercise of the address being represented as a
     * string, we will simply use the string itself as the returned hash for comparison, esp. to aid debugging.
     */
    public String hashableValue() {
        return getAddressString();
    }

}

