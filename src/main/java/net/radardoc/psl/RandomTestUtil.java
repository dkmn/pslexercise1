package net.radardoc.psl;

import java.util.*;

public class RandomTestUtil {

    // random generator initialization
    Random randomGenerator = new Random();

    // fraction of queries that come from "the small subset of addresses", default as per exercise
    public Float fractionFromFrequentQueriers = new Float(0.95f);
    // total number of Addresses to generate in a cohort, default size conveniently for testing
    public Integer numberOfTestQueries = new Integer(100000);


    /**
     * Generate a pseudo-random address.
     * @return
     */
    public Address getRandomAddress() {
        return new Address(getRandomAddressString());
    }

    /**
     * Generate a notional test cohort.
     * @return
     */
    public ArrayList<Address> getMixedTestAddressCohort() {

        System.out.println("Generating synthetic test cohort of " +
                this.numberOfTestQueries + " queries with " +
                this.fractionFromFrequentQueriers + " fraction of frequently-queried addresses."
        );

        ArrayList<Address> listToReturn = new ArrayList<Address>();

        // add a few fixed addresses, to simulate the "majority whence the queries will come"
        // NOTE: this is divided by 3 because we are explicitly putting 3 different recognizable addresses in
        int boundForFrequentAddresses =  Math.round((this.numberOfTestQueries.floatValue() * this.fractionFromFrequentQueriers) / 3.0f);
        for(int i=0; i<boundForFrequentAddresses; i++){
            listToReturn.add(new Address("280 Madison Ave. North, Bainbridge Island WA 98110"));
            listToReturn.add(new Address("1600 Pennsylvania Ave, Washington DC 20500"));
            listToReturn.add(new Address("240 2nd Ave S #300, Seattle WA 98104"));
        }

        // now some pseudo-random ones
        // insert a number that is essentially the remainder up to "numberOfTestQueries"
        int boundForInfrequentAddresses = this.numberOfTestQueries - 3 * boundForFrequentAddresses;
        for(int i=0; i<boundForInfrequentAddresses; i++) {
            listToReturn.add(new Address(getRandomAddressString()));
        }

        // Shuffle the list to simulate random ordering of queries!
        // NOTE: The _actual_ distribution of these over time will affect what kind of caching makes sense
        java.util.Collections.shuffle(listToReturn);

        System.out.println("Finished synthetic test cohort generation.");

        return listToReturn;
    }

    /**
     * Generate different (though not very creative) addresses for testing
     * ... basically randomize the street number
     * @return
     */
    private String getRandomAddressString() {
        return String.valueOf(this.randomGenerator.nextInt(12000)) +
                " Main Street, Anytown WA " +
                String.valueOf(this.randomGenerator.nextInt(30000) + 60000);
    }

}
