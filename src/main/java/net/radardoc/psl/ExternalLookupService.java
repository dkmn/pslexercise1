package net.radardoc.psl;

public class ExternalLookupService {

    private java.util.Random randomGenerator = new java.util.Random();

    /**
     * This is our minimally-abstracted entry point to call the "expensive" external method
     * (expensive in time, and the need to minimize # of calls as specified; this could also reflect Saas cost, latency,
     * poor UX due to slow response, etc.)
     *
     * This function returns a float with the tax rate as a unit less Float between 0.0 and 1.0
     *
     * @param addressToLookup
     * @return
     */
    public Float salesTaxLookup(Address addressToLookup) {
        // In reality, would do external system calls, likely an external REST or SOAP API for a tax service like Avalara

        // EXERCISE: for this exercise, need to simulate a rate; either hash or random process resulting in a float works.
        // (Random process works since we are focusing the cache de-duplication on the address, not the result, but this
        // is more opaque for troubleshooting).
        // Thus, practically, I set this to return a pseudo-random sales tax rate between 0 and 10% (Oregon and Washington, I suppose ;)
        // Will base this on a hash to get the same value for each address (aids troubleshooting); note this is 32 bits
        // so adding an offset to keep it positive (-100,100) + 100 -> (0,200)
        // Hope this is a bit less opaque than bit masking.
        return ( (addressToLookup.getAddressString().hashCode() % 100) + 100) / 2000.0f;
        }

}
