# a base image, with Java support, minimal
FROM openjdk:8-alpine

# add Alpine packages
RUN apk update

RUN apk add bash
RUN apk add curl
RUN apk add maven

# create a working directory
RUN mkdir -p /var/psl
ENV DIRPATH /var/psl
WORKDIR $DIRPATH

# Copy repo into container
COPY . $DIRPATH

# Build the project in the container
RUN mvn clean package

# Automatically run
ENTRYPOINT ["java", "-jar", "target/pslexercise1-1.0-SNAPSHOT.jar"]


